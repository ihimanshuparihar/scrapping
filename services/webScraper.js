const axios = require("axios");
const cheerio = require("cheerio");

async function scrapeStockInfo(stockName) {
  // const url = `https://finance.yahoo.com/quote/${stockName}`;
  const modifiedStockName = stockName.replace(/\s+/g, "-").toLowerCase();
  const url = `https://money.rediff.com/companies/${modifiedStockName}`;
  try {
    const response = await axios.get(url);
    const $ = cheerio.load(response.data);

    let stockInfo = {
      content: $("#leftcontainer")
        .text()
        .replace(/\s+/g, " ")
        .replace(/\n/g, ""),
    };

    return stockInfo;
  } catch (error) {
    console.error("Error scraping data:", url);
    return null;
  }
}

module.exports = {
  scrapeStockInfo,
};
