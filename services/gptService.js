require("dotenv").config();
const OpenAI = require("openai").OpenAI;
const openai = new OpenAI(
  "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"
);

async function generateText(stockInfo) {
  try {
    const response = await openai.chat.completions.create({
      messages: [
        {
          role: "user",
          content: `Summarize the following stock information in valid json format detail: ${stockInfo.content}.Do not include any explanations, only provide a  JSON response`,
          // content: `sumarize ${stockInfo.content} and give stock name ,current price, date,todays returns,todays profit/loss,realtime news array, previous close,Day's H/L (Rs.),52wk H/L (Rs.)Mkt Cap,Report Card in the JSON format`,
        },
      ],
      model: "gpt-3.5-turbo",
    });
    return JSON.parse(response.choices[0].message.content);
  } catch (error) {
    if (error.response) {
      console.error("API Error:", error.response.data);
    } else {
      console.error("Error generating text:", error);
    }
    return "Error generating text";
  }
}

module.exports = {
  generateText,
};
