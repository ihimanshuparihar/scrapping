const express = require("express");
const app = express();
const stockRoutes = require("./routes/stockRoutes");

app.use(express.json());

app.use("/api", stockRoutes);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
