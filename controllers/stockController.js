const webScraper = require("../services/webScraper");
const gptService = require("../services/gptService");

// Controller function to handle the stock data retrieval and text generation
async function getStockSummary(req, res) {
  const stockName = req.params.stockName;
  try {
    // Use the web scraper service to get stock information
    const stockInfo = await webScraper.scrapeStockInfo(stockName);

    if (stockInfo) {
      const summary = await gptService.generateText(stockInfo);
      res.json({ summary });
    } else {
      res.status(404).json({ error: "Stock not found" });
    }
    // res.json({ stockInfo });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
}

module.exports = {
  getStockSummary,
};
